#!/bin/bash

{
echo 'conf-dir=/etc/dnsmasq.d/,*.conf'
for domain in $MITM_DOMAINS; do
  echo "address=/$domain/${MITM_IP}"
done
} > /etc/dnsmasq.conf

dnsmasq -k
