FROM alpine:edge
RUN apk --no-cache add dnsmasq bash
COPY start.sh /start.sh
EXPOSE 53 53/udp
ENTRYPOINT ["/bin/bash"]
CMD ["/start.sh"]
